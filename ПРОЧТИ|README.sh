#mdru/Приступая к работе/Обзор
#mdru 123
#mdru ГитМД - это документация кода с помощью Маркдаун (Markdown).
#mdru 
#mdru От вас требуется:
#mdru 
#mdru 1. наличие хранилища (Гит, Меркуриал, иное);
#mdru 1. доступность файлов хранилища по HTTP(S);
#mdru 1. список файлов исходного кода, содержащих документацию.
#mdru 
#mdru Таким образом, вы можете документировать код практически на любом языке
#mdru с помощью известного и популярного формата Маркдаун без
#mdru каких-либо дополнительных действий вроде хостинга. Для получения
#mdru документации просто скормите ГитМД ссылку на список файлов.
#mdru 

#mden/Getting started/Overview
#mden 
#mden GitMD is code documentation with Markdown.
#mden 
#mden You need to have:
#mden 
#mden 1. a repository (Git, Mercurial, other);
#mden 1. repository files available over HTTP(S);
#mden 1. a list of source code files that contain documentation.
#mden 
#mden Thus, you can document your code in almost any language using
#mden famous and popular Markdown format without additional actions
#mden like hosting. To get documentation, just feed GitMD a link to
#mden a list of files.
#mden 
