
//mdru/Классы/ШаблонUIK
//mdru Класс `ШаблонUIK` задаёт внешний вид документации как у сайта
//mdru [UIkit](https://getuikit.com/docs/introduction).
//mdru 
//mdru По сути, `ШаблонUIK` предоставляет API поверх определённой комбинации
//mdru HTML и CSS.
//mdru 

//mden/Classes/UIKTemplate
//mden `UIKTemplate` class sets documentation style to look like the site of
//mden [UIkit](https://getuikit.com/docs/introduction).
//mden 
//mden In essence, `UIKTemplate` provides an API that incapsulates a certain
//mden combination of HTML and CSS.
//mden 

ШаблонUIK = function(название, html, json, css)
{

    //TODOMDRU
    this.создатьСобытия = function(мир)
    {
        this._создатьСобытияШапки(мир);
        this._создатьСобытияМеню(мир);
        this._создатьСобытияСодержимого(мир);
    };

    this._создать(название, html, json, css);

};
UIKTemplate = ШаблонUIK;


ШаблонUIK.prototype._создать = function(название, html, json, css)
{
    this.название = название;
    this.html = html;
    this.json = JSON.parse(json);
    this.css = css;

    this.константы = {
        "заголовок": `${название}-шаблон-uik-шапка-заголовок`,
        "версия": `${название}-шаблон-uik-шапка-версия`,
        "языки": `${название}-шаблон-uik-шапка-языки`,
        "настольное": `${название}-шаблон-uik-меню-настольное`,
        "мобильное": `${название}-шаблон-uik-меню-мобильное`,
        "содержимое": `${название}-шаблон-uik-содержимое`,
    };
    this.шаблоны = {
        "языкLiId": название + "-шаблон-uik-шапка-языки-li-${номер}",
        "языкAId": название + "-шаблон-uik-шапка-языки-a-${номер}",
        "пунктМенюLiId": название + "-шаблон-uik-меню-${тип}-li-${номер}",
        "пунктМенюAId": название + "-шаблон-uik-меню-${тип}-a-${номер}",
    };
    // Подготавливаем структуру: заменяем константы в макете HTML.
    for (var константа in this.константы)
    {
        var старое = "${" + константа + "}";
        var новое = this.константы[константа];
        this.html = this.html.replace(старое, новое);
    }

    // По умолчанию выбираем первый язык.
    this.язык = 0;
};

function _шаблонUIK_задатьСвойствоПолучения(имя, name, функция)
{
    Object.defineProperty(ШаблонUIK.prototype, имя, { get: функция });
    Object.defineProperty(ШаблонUIK.prototype, name, { get: функция });
}

function _шаблонUIK_задатьСвойство(имя, name, getter, setter)
{
    Object.defineProperty(ШаблонUIK.prototype, имя, { get: getter, set: setter });
    Object.defineProperty(ШаблонUIK.prototype, name, { get: getter, set: setter });
}

//mdru/Свойства класса ШаблонUIK/структура
//mdru Свойство `структура` содержит то-то и то-то

//mden/UIKTemplate class properties/structure
//mden `structure` property contains this and this

_шаблонUIK_задатьСвойствоПолучения(
    "структура",
    "structure",
    function() {
        return this.html;
    }
);

//TODOMDRU вид/style
_шаблонUIK_задатьСвойствоПолучения(
    "вид",
    "style",
    function() {
        return this.css;
    }
);

//TODOMDRU заголовок
_шаблонUIK_задатьСвойство(
    "заголовок",
    "title",
    function() {
        return $(`#${this.константы.заголовок}`).text();
    },
    function(значение) {
        $(`#${this.константы.заголовок}`).text(значение);
    }
);

//TODOMDRU версия
_шаблонUIK_задатьСвойство(
    "версия",
    "version",
    function() {
        return $(`#${this.константы.версия}`).text();
    },
    function(значение) {
        $(`#${this.константы.версия}`).text(значение);
    }
);

//TODOMDRU языки
_шаблонUIK_задатьСвойство(
    "языки",
    "languages",
    function() {
        return this._языки;
    },
    function(значение) {
        this._языки = значение;
        if (this.задалиЯзыки)
        {
            this.задалиЯзыки.уведомить();
        }
    }
);

//TODOMDRU язык
_шаблонUIK_задатьСвойство(
    "язык",
    "language",
    function() {
        return this._язык;
    },
    function(значение) {
        this._язык = значение;
        if (this.выбралиЯзык)
        {
            this.выбралиЯзык.уведомить();
        }
    }
);

//mdru/меню
_шаблонUIK_задатьСвойство(
    "меню",
    "menu",
    function() {
        return this._пунктыМеню;
    },
    function(значение) {
        this._пунктыМеню = значение;
        if (this.задалиМеню)
        {
            this.задалиМеню.уведомить();
        }
    }
);

//mdru/пунктМеню
_шаблонUIK_задатьСвойство(
    "пунктМеню",
    "menuItem",
    function() {
        return this._пунктМеню;
    },
    function(значение) {
        this._пунктМеню = значение;
        if (this.выбралиПунктМеню)
        {
            this.выбралиПунктМеню.уведомить();
        }
    }
);

//mdru/содержимое
_шаблонUIK_задатьСвойство(
    "содержимое",
    "contents",
    function() {
        return this._текст;
    },
    function(значение) {
        this._текст = значение;
        if (this.задалиСодержимое)
        {
            this.задалиСодержимое.уведомить();
        }
    }
);


// Шапка


ШаблонUIK.prototype._создатьСобытияШапки = function(мир)
{
    var тут = this;

    this.выбралиЗаголовок = new мир.Уведомитель();
    this.selectedTitle = this.выбралиЗаголовок;
    $(`#${this.константы.заголовок}`).click(function() {
        тут.выбралиЗаголовок.уведомить();
    });

    this.выбралиВерсию = new мир.Уведомитель();
    this.selectedVersion = this.выбралиВерсию;
    $(`#${this.константы.версия}`).click(function() {
        тут.выбралиВерсию.уведомить();
    });

    this.задалиЯзыки = new мир.Уведомитель();
    this.setLanguages = this.задалиЯзыки;
    this.задалиЯзыки.подписать(function() {
        тут._отобразитьЯзыки();
    });
    // Отобразить языки сразу.
    тут._отобразитьЯзыки();

    this.выбралиЯзык = new мир.Уведомитель();
    this.selectedLanguage = this.выбралиЯзык;
    this.выбралиЯзык.подписать(function() {
        тут._отобразитьВыбранныйЯзык();
    });
    // Отобразить выбранный язык сразу.
    тут._отобразитьВыбранныйЯзык();
};


ШаблонUIK.prototype._языкLiId = function(номер)
{
    return this.шаблоны.языкLiId.replace("${номер}", номер);
};

ШаблонUIK.prototype._языкAId = function(номер)
{
    return this.шаблоны.языкAId.replace("${номер}", номер);
};

ШаблонUIK.prototype._шаблонЯзыка = function(номер, язык)
{
    var liId = this._языкLiId(номер);
    var aId = this._языкAId(номер);
    return this.json.язык
        .replace("${название}", язык)
        .replace("${liId}", liId)
        .replace("${aId}", aId)
    ;
};

ШаблонUIK.prototype._отобразитьЯзыки = function()
{
    var содержимое = "";
    for (var номер in this._языки)
    {
        var язык = this._языки[номер];
        содержимое += this._шаблонЯзыка(номер, язык);
    }

    $(`#${this.константы.языки}`).html(содержимое);

    var тут = this;
    for (var номер in this._языки)
    {
        var aId = this._языкAId(номер);
        const номерЯзыка = номер;
        $(`#${aId}`).click(function() {
            тут.язык = номерЯзыка;
        });
    }
};

ШаблонUIK.prototype._отобразитьВыбранныйЯзык = function()
{
    // Очищаем.
    for (var номер in this._языки)
    {
        var liId = this._языкLiId(номер);
        $(`#${liId}`).removeClass("uk-active");
    }
    // Выбираем.
    var liId = this._языкLiId(this.язык);
    $(`#${liId}`).addClass("uk-active");
};


// Меню


ШаблонUIK.prototype._создатьСобытияМеню = function(мир)
{
    var тут = this;

    this.задалиМеню = new мир.Уведомитель();
    this.setMenu = this.задалиМеню;
    this.задалиМеню.подписать(function() {
        тут._отобразитьМеню();
    });
    // Отобразить меню сразу.
    тут._отобразитьМеню();

    this.выбралиПунктМеню = new Уведомитель();
    this.selectedMenuItem = this.выбралиПунктМеню;
    this.выбралиПунктМеню.подписать(function() {
        тут._отобразитьВыбранныйПунктМеню();
    });
    // Отобразить выбранный пункт меню сразу.
    тут._отобразитьВыбранныйПунктМеню();
};

ШаблонUIK.prototype._содержимоеМеню = function(тип)
{
    var содержимое = "";
    for (var номер in this._пунктыМеню)
    {
        var пункт = this._пунктыМеню[номер];
        var этоЗаголовок = пункт.startsWith("# ");
        if (этоЗаголовок)
        {
            var заголовок = пункт.substring(2);
            содержимое += this._шаблонЗаголовкаМеню(заголовок);
        }
        else
        {
            содержимое += this._шаблонПунктаМеню(тип, номер, пункт);
        }
    }
    return содержимое;
};

ШаблонUIK.prototype._отобразитьМеню = function()
{
    $(`#${this.константы.настольное}`).html(this._содержимоеМеню("настольное"));
    $(`#${this.константы.мобильное}`).html(this._содержимоеМеню("мобильное"));

    var тут = this;
    for (var номер in this._пунктыМеню)
    {
        const номерПункта = номер;
        var aId = this._пунктМенюAId("настольное", номер);
        $(`#${aId}`).click(function() {
            тут.пунктМеню = номерПункта;
        });
        var aId = this._пунктМенюAId("мобильное", номер);
        $(`#${aId}`).click(function() {
            тут.пунктМеню = номерПункта;
        });
    }
};

ШаблонUIK.prototype._пунктМенюLiId = function(тип, номер)
{
    return this.шаблоны.пунктМенюLiId
        .replace("${тип}", тип)
        .replace("${номер}", номер)
    ;
};

ШаблонUIK.prototype._пунктМенюAId = function(тип, номер)
{
    return this.шаблоны.пунктМенюAId
        .replace("${тип}", тип)
        .replace("${номер}", номер)
    ;
};

ШаблонUIK.prototype._шаблонЗаголовкаМеню = function(заголовок)
{
    return this.json.заголовокМеню.replace("${название}", заголовок);
};

ШаблонUIK.prototype._шаблонПунктаМеню = function(тип, номер, пункт)
{
    var liId = this._пунктМенюLiId(тип, номер);
    var aId = this._пунктМенюAId(тип, номер);
    return this.json.пунктМеню
        .replace("${название}", пункт)
        .replace("${liId}", liId)
        .replace("${aId}", aId)
    ;
};

ШаблонUIK.prototype._отобразитьВыбранныйПунктМеню = function()
{
    // Очищаем.
    for (var номер in this._пунктыМеню)
    {
        var пункт = this._пунктыМеню[номер];
        // Пропускаем заголовки.
        var этоЗаголовок = пункт.startsWith("# ");
        if (этоЗаголовок)
        {
            continue;
        }
        var настольноеLiId = this._пунктМенюLiId("настольное", номер);
        var мобильноеLiId = this._пунктМенюLiId("мобильное", номер);
        $(`#${настольноеLiId}`).removeClass("uk-active");
        $(`#${мобильноеLiId}`).removeClass("uk-active");
    }
    // Выбираем.
    var настольноеLiId = this._пунктМенюLiId("настольное", this.пунктМеню);
    var мобильноеLiId = this._пунктМенюLiId("мобильное", this.пунктМеню);
    $(`#${настольноеLiId}`).addClass("uk-active");
    $(`#${мобильноеLiId}`).addClass("uk-active");
};


// Содержимое.


ШаблонUIK.prototype._создатьСобытияСодержимого = function(мир)
{
    var тут = this;

    this.задалиСодержимое = new мир.Уведомитель();
    this.setContents = this.задалиСодержимое;
    this.задалиСодержимое.подписать(function() {
        тут._отобразитьСодержимое();
    });
    // Отобразить содержимое сразу.
    тут._отобразитьСодержимое();
};

ШаблонUIK.prototype._отобразитьСодержимое = function()
{
    $(`#${this.константы.содержимое}`).html(this.содержимое);
};
